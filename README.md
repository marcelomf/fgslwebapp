FgslWebApp
==========

Este projeto visa construção de uma web app do Evento FGSL (Fórum Goiano de Software Livre) http://fgsl.aslgo.org.br

Ainda está em fase de desenvolvimento, obtenha na loja que mais combina com você:

* Play Store - https://play.google.com/store/apps/details?id=br.org.aslgo.fgsl

* Firefox MarketPlace - https://marketplace.firefox.com/app/fgsl/

* Chrome Webstore - https://chrome.google.com/webstore/detail/fgsl/ghihhfoahkdmfpfjapbnnfheiklfbojm?hl=pt-BR

* Web - http://fgsl.ledlabs.com.br
